self: super:

{
  nix = super.nix.overrideAttrs (old: {
    patches = (old.patches or [])
      ++ [ ./0001-nix-build-Add-NIX_EXEC_SHELL_HOOK-to-skip-shellHook.patch ];
  });
}
