self: super:

{
  i3status-rust = super.i3status-rust.overrideAttrs (_: {
    patches = [
      (self.fetchpatch {
        url = https://github.com/greshake/i3status-rust/commit/1475ebecf83528e2b729b5ec925f2bafaee6b890.patch;
        sha256 = "0zmzb644glcsz1jgv9fs49kia4nma0yfd4y6mb069vng5kgm7qxn";
      })
    ];
  });
}
