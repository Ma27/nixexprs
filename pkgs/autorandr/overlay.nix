self: super:

{
  autorandr = super.autorandr.overrideAttrs (_: {
    patches = [
      (self.fetchpatch {
        url = https://github.com/Ma27/autorandr/commit/d38e22e5064d448099d1925cce16593a327c7d74.patch;
        sha256 = "1w5iay8gppyz6gz4b8vdkc1zp2m9j2bahmhfsx8mjdg4n9gq8pzh";
      })
    ];
  });
}
