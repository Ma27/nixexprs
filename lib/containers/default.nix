{ lib }: with lib;

rec {
  /*
    The actual container names in are the first 7 letters
    of the node in the deployment by default.
   */
  node2container = substring 0 7;

  /*
    Searches for available containers in the deployment configuration (`resources.machines`)
    and returns all nodes that appear to be container deployments.
   */
  containers = machines: attrNames
    (filterAttrs (_: v: v.deployment.targetEnv == "container") machines);
}
