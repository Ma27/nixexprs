{ callPackage ? null }:

let

  callPackage' = assert callPackage != null; callPackage;

  containerLib = callPackage' ./lib/containers { };

in

rec {

  nurPath = ./.;

  modules = {
    hydra = import ./modules/hydra.nix;
    sieve-dsl = import ./modules/sieve-dsl.nix;
  };

  ### OVERLAYS
  overlays = {
    sudo = import ./pkgs/sudo/overlay.nix;
    hydra = import ./pkgs/hydra/overlay.nix;
    php = import ./pkgs/php/overlay.nix;
    fzf-zsh = import ./pkgs/fzf-zsh/overlay.nix;
    fzf-nix-helpers = import ./pkgs/fzf-helpers/overlay.nix;
    autorandr = import ./pkgs/autorandr/overlay.nix;
    nix = import ./pkgs/nix/overlay.nix;
    i3status-rust = import ./pkgs/i3status-rust/overlay.nix;
  };

  ### PACKAGES
  gianas-return = callPackage' ./pkgs/gianas-return { };

  fzf-zsh = callPackage' ./pkgs/fzf-zsh { };

  fzf-nix-helpers = callPackage' ./pkgs/fzf-helpers { };

  ### LIBRARY
  mkTexDerivation = callPackage' ./lib/tex/make-tex-env.nix { };

  checkoutNixpkgs = callPackage' ./lib/release/checkout-nixpkgs.nix { };

  mkJob = callPackage' ./lib/release/mk-job.nix { };

  mkTests = callPackage' ./tests/mk-test.nix { };

  mkJobset = callPackage' ./lib/release/hydra-config.nix { };

  callNURPackage = callPackage';

  inherit (containerLib) node2container containers gen-firewall;

}
