{ lib, node2container, containers, gen-firewall }:

lib.runTests {

  testNode2Container = {
    expr = node2container "hartzfor";
    expected = "hartzfo";
  };

  testContainers = {
    expr =
      let deployment = { a.deployment.targetEnv = "container"; b.deployment.targetEnv = "none"; };
      in
        containers deployment;

    expected = [ "a" ];
  };

}
